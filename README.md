
# Mohammad Inanloo's Portfolio

Welcome to the repository for my personal portfolio website. This site showcases my skills as a Senior Fullstack Developer and DevOps Engineer, including my work with top brands, my educational background, and the projects I have worked on.

## Features

- Responsive web design
- Interactive skills charts
- Portfolio of significant projects
- Contact form integrated
- Smooth animations and transitions

## Technologies Used

- HTML5
- CSS3
- JavaScript
- Bootstrap
- SwiperJS for sliders
- Font Awesome for icons
- ProgressBar.js for interactive skill charts
- jQuery

## Project Structure

```plaintext
portfolio/
│
├── css/                   # Stylesheets folder for CSS files
│   ├── plugins/           # CSS plugins
│   │   ├── bootstrap.min.css
│   │   ├── font-awesome.min.css
│   │   └── swiper.min.css
│   └── style.css          # Main stylesheet
│
├── img/                   # Folder for images
│
├── js/                    # Scripts folder
│   ├── plugins/           # JavaScript plugins
│   │   ├── jquery.min.js
│   │   ├── swiper.min.js
│   │   └── ...            # Other JS plugins
│   └── main.js            # Main JavaScript file
│
└── index.html             # Main HTML file
```

## Setup

To set up this project locally, you'll need to clone the repository and open `index.html` in your web browser. Here's how you can clone the repo:

```bash
git clone https://example.com/path/to/repo.git
cd portfolio
# Open index.html in your browser
```

## Contributing

I'm open to contributions to improve this portfolio. If you have suggestions or improvements, please fork the repository and submit a pull request.

## License

This project is open-sourced under the MIT license.

## Contact

Should you have any questions, feel free to contact me directly through my [LinkedIn](https://www.linkedin.com/in/radiophp/) profile or via the contact form on the website.

---

Thank you for visiting my portfolio repository!
