$(function () {
    "use strict";
    // swup js
    const options = {
        containers: ["#swup", "#swupMenu"], animateHistoryBrowsing: true,
    };
    const swup = new Swup(options);
    // scrollbar
    Scrollbar.use(OverscrollPlugin);
    Scrollbar.init(document.querySelector('#scrollbar'), {
        damping: 0.05, renderByPixel: true, continuousScrolling: true,
    });
    Scrollbar.init(document.querySelector('#scrollbar2'), {
        damping: 0.05, renderByPixel: true, continuousScrolling: true,
    });
    // page loading
    $(document).ready(function () {
        anime({
            targets: '.mohi-preloader .mohi-preloader-content',
            opacity: [0, 1],
            delay: 200,
            duration: 600,
            easing: 'linear',
            complete: function (anim) {
            }
        });
        anime({
            targets: '.mohi-preloader',
            opacity: [1, 0],
            delay: 2200,
            duration: 400,
            easing: 'linear',
            complete: function (anim) {
                $('.mohi-preloader').css('display', 'none');
            }
        });
    });
    var bar = new ProgressBar.Line(preloader, {
        strokeWidth: 1.7, easing: 'easeInOut', duration: 1400, delay: 750, trailWidth: 1.7, svgStyle: {
            width: '100%', height: '100%'
        }, step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
        }
    });
    bar.animate(1);
    // counters
    anime({
        targets: '.mohi-counter-frame', opacity: [0, 1], duration: 800, delay: 2300, easing: 'linear',
    });
    anime({
        targets: '.mohi-counter', delay: 1300, opacity: [1, 1], complete: function (anim) {
            $('.mohi-counter').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 2000, easing: 'linear', step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
    });
// Function to create and animate a progress bar
    function createProgressBar(element, delay, animateTo) {
        new ProgressBar.Circle(element, {
            strokeWidth: 7,
            easing: 'easeInOut',
            duration: 1400,
            delay: delay,
            trailWidth: 7,
            step: function (state, circle) {
                var value = Math.round(circle.value() * 100);
                circle.setText(value === 0 ? '' : value);
            }
        }).animate(animateTo);
    }
// Create progress bars with specific settings
    createProgressBar(circlelang1, 2500, 1);   // Animate to 100%
    createProgressBar(circlelang2, 2600, 0.8); // Animate to 80%
    createProgressBar(circlelang3, 2700, 0.1); // Animate to 10%
// Function to create and animate a line progress bar
    function createLineProgressBar(element, delay, animateTo) {
        new ProgressBar.Line(element, {
            strokeWidth: 1.72, easing: 'easeInOut', duration: 1400, delay: delay, trailWidth: 1.72, svgStyle: {
                width: '100%', height: '100%'
            }, step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + ' %');
            }
        }).animate(animateTo);
    }
// Create and animate line progress bars with specific settings
// Initialize progress bars for each skill with assumed expertise levels
    createLineProgressBar(document.getElementById('lineprogphp'), 3100, 0.95);           // PHP & MVC Frameworks
    createLineProgressBar(document.getElementById('lineproglaravel'), 3200, 0.85);       // Laravel & Lumen
    createLineProgressBar(document.getElementById('lineprogjs'), 3300, 0.75);            // Node.js, Next.js, React.js, Redux
    createLineProgressBar(document.getElementById('lineprogpython'), 3400, 0.45);        // Python
    createLineProgressBar(document.getElementById('lineprogcppsql'), 3500, 0.55);       // Visual C++ & Microsoft SQL Server
    createLineProgressBar(document.getElementById('lineprogserver'), 3600, 0.95);        // Apache & Nginx
    createLineProgressBar(document.getElementById('lineprogwebservices'), 3700, 0.85);   // Web Services
    createLineProgressBar(document.getElementById('lineprogfrontend'), 3800, 0.95);      // Front-end Technologies
    createLineProgressBar(document.getElementById('lineprogsql'), 3900, 0.90);           // MySQL & PostgreSQL
    createLineProgressBar(document.getElementById('lineprognosql'), 4000, 0.75);         // NoSQL Databases
    createLineProgressBar(document.getElementById('lineprogdevops'), 4100, 0.85);        // DevOps & Cloud Services
    createLineProgressBar(document.getElementById('lineprogemergingtech'), 4200, 0.60);  // Emerging Technologies
    // Contact form
    $('.mohi-input').keyup(function () {
        if ($(this).val()) {
            $(this).addClass('mohi-active');
        } else {
            $(this).removeClass('mohi-active');
        }
    });
    $("#form").submit(function () {
        $.ajax({
            type: "POST", url: "mail.php", data: $(this).serialize()
        }).done(function () {
            var tl = anime.timeline({
                easing: 'easeOutExpo',
            });
            tl
                .add({
                    targets: '.mohi-submit', opacity: 0, scale: .5,
                })
                .add({
                    targets: '.mohi-success', scale: 1, height: '45px',
                })
        });
        return false;
    });
    var swiper = new Swiper('.mohi-blog-slider', {
        slidesPerView: 3, spaceBetween: 30, speed: 1400, autoplay: {
            delay: 4000,
        }, autoplaySpeed: 5000, pagination: {
            el: '.swiper-pagination', clickable: true,
        }, navigation: {
            nextEl: '.mohi-blog-swiper-next', prevEl: '.mohi-blog-swiper-prev',
        }, breakpoints: {
            1500: {
                slidesPerView: 3,
            }, 1200: {
                slidesPerView: 2,
            }, 992: {
                slidesPerView: 1,
            },
        },
    });
    $('.current-menu-item a').clone().appendTo('.mohi-current-page');
    $('.mohi-map-overlay').on('click', function () {
        $(this).addClass('mohi-active');
    });
    $('.mohi-info-bar-btn').on('click', function () {
        $('.mohi-info-bar').toggleClass('mohi-active');
        $('.mohi-menu-bar-btn').toggleClass('mohi-disabled');
    });
    $('.mohi-menu-bar-btn').on('click', function () {
        $('.mohi-menu-bar-btn , .mohi-menu-bar').toggleClass("mohi-active");
        $('.mohi-info-bar-btn').toggleClass('mohi-disabled');
    });
    $('.mohi-info-bar-btn , .mohi-menu-bar-btn').on('click', function () {
        $('.mohi-content').toggleClass('mohi-active');
    });
    $('.mohi-curtain , .mohi-mobile-top-bar').on('click', function () {
        $('.mohi-menu-bar-btn , .mohi-menu-bar , .mohi-info-bar , .mohi-content , .mohi-menu-bar-btn , .mohi-info-bar-btn').removeClass('mohi-active , mohi-disabled');
    });
    $('.menu-item').on('click', function () {
        if ($(this).hasClass('menu-item-has-children')) {
            $(this).children('.sub-menu').toggleClass('mohi-active');
        } else {
            $('.mohi-menu-bar-btn , .mohi-menu-bar , .mohi-info-bar , .mohi-content , .mohi-menu-bar-btn , .mohi-info-bar-btn').removeClass('mohi-active , mohi-disabled');
        }
    });
    // reinit
    document.addEventListener("swup:contentReplaced", function () {
        Scrollbar.use(OverscrollPlugin);
        Scrollbar.init(document.querySelector('#scrollbar'), {
            damping: 0.05, renderByPixel: true, continuousScrolling: true,
        });
        Scrollbar.init(document.querySelector('#scrollbar2'), {
            damping: 0.05, renderByPixel: true, continuousScrolling: true,
        });
        $("#form").submit(function () {
            $.ajax({
                type: "POST", url: "mail.php", data: $(this).serialize()
            }).done(function () {
                var tl = anime.timeline({
                    easing: 'easeOutExpo',
                });
                tl
                    .add({
                        targets: '.mohi-submit', opacity: 0, scale: .5,
                    })
                    .add({
                        targets: '.mohi-success', scale: 1, height: '45px',
                    })
            });
            return false;
        });
        anime({
            targets: '.mohi-counter-frame', opacity: [0, 1], duration: 800, delay: 300, easing: 'linear',
        });
        $('.mohi-counter').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000, easing: 'linear', step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        // slider testimonials
        var swiper = new Swiper('.mohi-testimonial-slider', {
            slidesPerView: 3, spaceBetween: 30, speed: 1400, autoplay: false, autoplaySpeed: 5000, pagination: {
                el: '.swiper-pagination', clickable: true,
            }, navigation: {
                nextEl: '.mohi-testi-swiper-next', prevEl: '.mohi-testi-swiper-prev',
            }, breakpoints: {
                1500: {
                    slidesPerView: 2,
                }, 1200: {
                    slidesPerView: 2,
                }, 992: {
                    slidesPerView: 2,
                }, 768: {
                    slidesPerView: 1,
                }
            },
        });
        // slider blog
        var swiper = new Swiper('.mohi-blog-slider', {
            slidesPerView: 3, spaceBetween: 30, speed: 1400, autoplay: {
                delay: 4000,
            }, autoplaySpeed: 5000, pagination: {
                el: '.swiper-pagination', clickable: true,
            }, navigation: {
                nextEl: '.mohi-blog-swiper-next', prevEl: '.mohi-blog-swiper-prev',
            }, breakpoints: {
                1500: {
                    slidesPerView: 3,
                }, 1200: {
                    slidesPerView: 3,
                }, 992: {
                    slidesPerView: 1,
                },
            },
        });
        $('.current-menu-item a').clone().prependTo('.mohi-current-page');
        $('.menu-item').on('click', function () {
            if ($(this).hasClass('menu-item-has-children')) {
                $(this).children('.sub-menu').toggleClass('mohi-active');
            } else {
                $('.mohi-menu-bar-btn , .mohi-menu-bar , .mohi-info-bar , .mohi-content , .mohi-menu-bar-btn , .mohi-info-bar-btn').removeClass('mohi-active , mohi-disabled');
            }
        });
    })
});
